import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '../assets/css/all.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#498EC5',
        secondary: '#B6D0DE',
        accent: '#8c9eff',
        error: '#b71c1c',
      },
    },
  },
});
