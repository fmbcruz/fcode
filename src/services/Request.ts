import axios from 'axios';
/* eslint no-console: ["error", { allow: ["warn", "error"] }] */


export default class Request {

  request(route) {
    //let userToken = localStorage.getItem('token') ? localStorage.getItem('token') :  "";
    let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjMyNWQ1YTRjMWJiMmI1YjBjZmE1ZWU1ODY0MDY2MDFjOGFiMjFmMDc0ZGI0NmQ4YmFjNDQ1NmMyYzViZmM2ZGJkYmNkNGU3ODNhOWE0YTdkIn0.eyJhdWQiOiIzIiwianRpIjoiMzI1ZDVhNGMxYmIyYjViMGNmYTVlZTU4NjQwNjYwMWM4YWIyMWYwNzRkYjQ2ZDhiYWM0NDU2YzJjNWJmYzZkYmRiY2Q0ZTc4M2E5YTRhN2QiLCJpYXQiOjE1Nzk3MzQ3ODYsIm5iZiI6MTU3OTczNDc4NiwiZXhwIjoxNjExMzU3MTg2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WbyTHRelm3jEOtBtnpw4l2YdG345yJ1LRiZv-z3B9__jRrV41NzeyPxVgDJyc3ssBflKydNG8isiAAJNAsVFn_y8taURYN5BQK8IolcN35cSx_yVjyfYrBEMmORizQ-u2Nw6bzYadQPK8Lg7y1K1kGVcPp7bgCmt4R5s4xrSZWE_3pdachhhtlhEieTwlB0Kf259a6Nm-Cn4zLgpDffJCzxqy7tv-UAnCBo_Xa2zPj1GKxG2IEC_e8CfgOSnWFdInVtNihXoaqu78QvKQQhSSBNzH9Azw2a-YDk1V4GMlHkLrK5gSY7PdH38cE-cUohBMoZ7k3TVx2pR0q_WI6BaE2uiAY9IBkDRgnHfPy1iTy6_-Nc_LJiJOvVa-sn_j5xMxXbTQZwm2J70_Ot4Xx0WgowXj0GDX-K--YTAPJmeFXcw1u9_HOBNf74APMQ2wmHENC2-M0M9EI13omBeOwL832lUZFVMawiEha-9ihmlMXk2kUc5Qi8pydHNrEI05Io6WEc5vxMxu8YwSrkA18ogfzvi0ddsuluzcxWJ9FqH5K7yM95N7g3NajvdHCOrVbV238NAB8dFTAMfFImLG5DZIdwkZHtwue6xQ4yfSUTKdpI6syLIhFuQyjivZI2uSc8LqQ6Wkjp6FGM3oG_jp09jFYoHsGVS2O2NZ-giDnoi1hI";
    axios.defaults.baseURL = 'http://localhost:8000/api/';
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    console.warn(axios.defaults.headers)
    axios
      .get(route, {
        headers: {
          Authorization: `Bearer ${token}`,
        }
      })
      .then(response => {
        // If request is good...
        console.warn(response.data)
      })
      .catch((error) => {
        console.error(error.response.headers)
      })
  }

}