import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router';

Vue.config.productionTip = false

window.apiUrl = 'http://localhost:8000/api/';

/* eslint no-console: ["error", { allow: ["warn", "error"] }] */
  // console.warn((Vue.url).substr(1)); 

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')