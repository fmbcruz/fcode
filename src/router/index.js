import Vue from 'vue'
import VueRouter from 'vue-router'
import CreatePage from '../views/admin/Dashboard.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/experience/:id',
    name: 'experience',
    component: () => import('../views/Experience.vue')
  },
  {
    path: '/admin',
    name: 'admin',
    children: [
      {
        path: 'posts',
        name: 'posts',
        children: [
          {
            path: 'create',
            name: 'create',
            component: CreatePage
          }
        ],
        component: () => import('../views/admin/Pages.vue')
      },
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('../views/admin/Dashboard.vue')
      },
      {
        path: 'users',
        name: 'users',
        component: () => import('../views/admin/Dashboard.vue')
      },
    ],
    component: () => import('@/components/dashboard/Admin.vue'),
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/admin/Login.vue'),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('token')) next('admin/dashboard')
      else next()
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  scrollBehavior: function (to) {
    if (to.hash) {
      return { selector: to.hash }
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes
})

export default router
